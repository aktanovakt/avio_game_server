from django.db import models

from apps.users.models import (
    User,
)

class Telemetry(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='telemetry', to_field='username')
    telemetry = models.FileField(verbose_name='telemetry', upload_to='media/telemetry')
    telemetry_graf = models.FileField(verbose_name='telemetry grafic', upload_to='media/telemetry_graf')
    created_at = models.DateTimeField(verbose_name='created at', auto_now_add=True)

    def __str__(self) -> str:
        return f'{self.user.username}-{self.created_at}'