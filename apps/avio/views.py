from rest_framework.viewsets import ModelViewSet


from apps.avio.models import (
    Telemetry,
)

from apps.avio.serializers import (
    TelemetrySerializer,
)


class TelemetryView(ModelViewSet):
    queryset = Telemetry.objects.all()
    serializer_class = TelemetrySerializer