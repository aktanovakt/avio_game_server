from django.urls import path
from rest_framework.routers import DefaultRouter as DR
from apps.avio.views import (
    TelemetryView,
)


router = DR()   

router.register('telemetry', TelemetryView, basename='telemetry')

urlpatterns = [
    
]

urlpatterns += router.urls