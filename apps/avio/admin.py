from django.contrib import admin

from apps.avio.models import (
    Telemetry,
)

admin.site.register(Telemetry)