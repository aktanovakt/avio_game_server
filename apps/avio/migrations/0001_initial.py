# Generated by Django 4.2 on 2023-04-18 16:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Telemetry',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('telemetry', models.FileField(upload_to='media/telemetry', verbose_name='telemetry')),
                ('telemetry_graf', models.FileField(upload_to='media/telemetry_graf', verbose_name='telemetry grafic')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
            ],
        ),
    ]
