from rest_framework.serializers import ModelSerializer

from apps.avio.models import (
    Telemetry,
)


class TelemetrySerializer(ModelSerializer):
    class Meta:
        model = Telemetry
        fields = ('id', 'user', 'telemetry', 'telemetry_graf')
    