from django.urls import path

from apps.users.views import (
    RegistrationView, ActivationView, ExistsView
)


urlpatterns = [
    path('registration/', RegistrationView.as_view(), name='Registration'),
    path('activation/', ActivationView.as_view(), name='Actiavtion'),
    path('exists/<str:username>/', ExistsView.as_view(), name='Exists'),
]