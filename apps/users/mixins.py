from apps.users.models import (
    User,
)

from rest_framework.response import Response

class ExistsMixin:
    def get(self, request, username):
        if not User.objects.get(username=username):
            return Response('User does not exist')
        return Response('OK')
