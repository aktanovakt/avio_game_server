from rest_framework.generics import CreateAPIView
from apps.users.models import User
from rest_framework.views import APIView


from apps.users.serializers import (
    RegistrationSerializer, ActivationSerializer
)

from apps.users.mixins import (
    ExistsMixin,
)



class RegistrationView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegistrationSerializer



class ActivationView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = ActivationSerializer



class ExistsView(ExistsMixin, APIView):
    pass