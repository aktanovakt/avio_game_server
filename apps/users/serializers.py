from rest_framework import serializers
from apps.users.models import User
import random


class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'password', 'username')


    def create(self, validated_data):
        password = validated_data.get('password')
        user = User.objects.create(**validated_data, code=str(random.randint(1000, 9999)))
        user.set_password(password)
        user.save()
        return user





class ActivationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(write_only=True)
    code = serializers.CharField(write_only=True)


    class Meta:
        model = User
        fields = ('id', 'email', 'code', 'is_active')


    def create(self, validated_data):
        user = User.objects.filter(email=validated_data.get('email')).first()
        if not user:
            raise serializers.ValidationError({'msg': 'User not found'})
        if user.is_active:
            raise serializers.ValidationError({'msg': 'You already is active'})
        if user.code == validated_data.get('code'):
            user.is_active = True
        user.save()
        return user


