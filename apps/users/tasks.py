from django.core.mail import send_mail
from celery import shared_task
from core.settings import EMAIL_HOST_USER


@shared_task
def send_code_to_email(instance):
    message = f'Your activation code: {instance.code}'
    send_mail('Actiavtion code', message, EMAIL_HOST_USER, [instance.email])



   