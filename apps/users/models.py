from django.db import models
from django.contrib.auth.models import AbstractUser
from apps.users.managers import CustomUserManager
from django.db.models.signals import post_save
from django.dispatch import receiver
from apps.users.tasks import send_code_to_email



class User(AbstractUser):
    email = models.EmailField(verbose_name='email', unique=True)
    username = models.CharField(verbose_name='username', max_length=40, unique=True)
    code = models.CharField(max_length=6, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    is_active = models.BooleanField(default=False)


    objects = CustomUserManager()
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self) -> str:
        return f'{self.email}'


@receiver(post_save, sender=User)
def send_email_code(sender, instance, created, **kwargs):
    if created:
        send_code_to_email(instance)
