## AVIO-GAME


### Для дольнейшей работы переименуйте .env.example на .env

### Запуск проекта с req + venv:
```
python3 -m venv venv
source venv/bin/activate
pip intsall -r requirements.txt
./manage.py runserver 8000
```


### Запуск проекта c использованием poetry:
```
poetry shell
poetry install
poetry update
./manage.py runserver 8000
```

### Команда для миграции после изменения моделей:
```
./manage.py makemigrations
./manage.py migrate
```

### Для создания админа:
#### Через админку можно просмотреть телеметрию
```
./manage.py createsuperuser
```

### Для проверки через swagger:
[Swagger - документация](http://localhost:8000/swagger/)